public class Krieger {
    protected int lebenspunkte;
    protected String name;
    protected Schwert schwert;
    protected FortgeschrittenerKampf fortgeschrittenerKampf;

    public Krieger(int lebenspunkte, String name)
    {
        this.lebenspunkte = lebenspunkte;
        this.name = name;
        this.schwert = null;

    }
    // setter Methode die ein Obejekt wenn man es aufruft Setzen kann!
    public void setSchwert(Schwert schwert)
    {
        this.schwert = schwert;
    }


    public void kriegerHautDrauf()
    {
        for (int i = 1; i<=10; i++ )
        {
            this.schwert.hauDrauf();

        }

    }

    public void veringereLeben(int schaden)

    {

        this.lebenspunkte = this.lebenspunkte - schaden;
        if(this.lebenspunkte < 0)
        {
            this.lebenspunkte = 0;
        }

    }

    public int getLebenspunkte() {
        return lebenspunkte;
    }

    public String getName() {

        return name;
    }



}
