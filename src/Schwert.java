public class Schwert {
    protected String name;
    protected String farbe;
    protected int schaden = 10;

    public Schwert()
    {
        this.name = "unbekannt";
        this.farbe = "unbekannt";


    }

    public void hauDrauf()
    {
        //System.out.println("Das " + this.farbe + "Schwert Namens: " + this.name + "haut dich kaputt" );

        this.schaden = 10;
    }


    public void setFarbe(String farbe)
    {
        this.farbe = farbe;

    }

    public void setName(String name)
    {
        this.name = name;

    }

    public int getSchaden() {
        return schaden;
    }
}
