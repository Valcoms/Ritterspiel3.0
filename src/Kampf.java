import java.util.Random;
import java.util.ArrayList;

public class Kampf {
    //protected heißt das er auf seine eigene Variablen zugreift so das man sie immer verwenden kann auch andere für sich selbst!
    // "Krieger kampfKrieger" heißt quasi das die Klasse Krieger in eine Variable kampfKrieger gepackt wird!
    // so kann man auf die Methoden und Objekte innerhalb der Klasse Krieger oder Magier zugreifen.
    protected Random random = new Random();
    protected ArrayList<Object> gewinnerList;
    protected Krieger krieger;
    protected Magier magier;
    //public Object magier;
    protected Krieger kampfKrieger;
    protected Magier kampfMagier;

    public Object macheSchaden(Magier kampfKrieger, Magier kampfMagier) {
        int count = 0;

        while (kampfMagier.getLebenspunkte() > 0 && kampfKrieger.getLebenspunkte() > 0) {
            int random = this.random.nextInt(10);

            if (random >= 5) {
                int kriegerSchaden = krieger.schwert.getSchaden();
                kampfMagier.veringereLeben(kriegerSchaden);
                System.out.println("Krieger " + kampfKrieger.getName() + " macht " + kriegerSchaden + " an " + kampfMagier.getName() + "\n");
                System.out.println("Magier Lebensstand: " + kampfMagier.getLebenspunkte());

            } else {
                System.out.println("Daneben!");
            }

            if (random <= 5) {
                int magierSchaden = magier.stab.getSchaden();
                kampfKrieger.veringereLeben(magierSchaden);
                System.out.println("Magier " + kampfMagier.getName() + " macht " + magierSchaden + " an " + kampfKrieger.getName() + "\n");
                System.out.println("Krieger Lebensstand: " + kampfKrieger.getLebenspunkte());
            } else {
                System.out.println("Daneben!");
            }
            //count++;
            //if (count > 20) {
            //     break;
            // }

        }
        if (kampfMagier.getLebenspunkte() == 0) {
            System.out.println("Der Magier " + kampfMagier.getName() + " ist tot!\n" + " Der Krieger " + kampfKrieger.getName() + " hat " + kampfKrieger.getLebenspunkte() + " Lebenspunkte");
            return kampfMagier;
        }

        if (kampfKrieger.getLebenspunkte() == 0) {
            System.out.println("Der Krieger " + kampfKrieger.getName() + " ist tot!\n" + " Der Magier " + kampfMagier.getName() + " hat " + kampfMagier.getLebenspunkte() + " Lebenspunkte");
            return kampfMagier;
        }

        return null;

    }


    public ArrayList<Object> KampfGegeneinander(ArrayList<Object> namenInListe)
    {
        while(namenInListe.size() > 2);
        {
            int counter = 0;
            for (Object figur : namenInListe) {
                //instanceof wenn figur instanz von Krieger ist!
                if (figur instanceof Krieger) {
                    //Object krieger = figur;
                    Object krieger = (Krieger) figur;
                    namenInListe.remove(counter);

                } else if (figur instanceof Magier) {
                    //Object magier = figur;
                    Object magier= (Magier) figur;
                    namenInListe.remove(counter);

                }
                counter++;

            }

            //Kampf kampf = new Kampf();
            Object gewinner = magier,krieger;
            gewinnerList.add(gewinner);
        }
        return gewinnerList;

    }

}










/***
* "Debugger"
* System.out.println("Magier: " + this.kampfMagier.getLebenspunkte());
System.out.println("Krieger: " + this.kampfKrieger.getLebenspunkte());
System.out.println("Random: " + random);
***/












/***
 * das war ein TEST :P
 */

/***
 while(this.kampfMagier.getLebenspunkte()>=0)

 if (this.random.nextInt(10) >= 5) {

 int kriegerSchaden = this.kampfKrieger.schwert.getSchaden();
 this.kampfMagier.veringereLeben(kriegerSchaden);
 System.out.println("Krieger " + this.kampfKrieger.getName() + " macht " + kriegerSchaden + " an " + this.kampfMagier.getName() + "\n");
 System.out.println(this.kampfMagier.getLebenspunkte());
 }

 else
 {
 System.out.println("Daneben!\n");
 }

 if (this.kampfMagier.getLebenspunkte() <=0)
 {
 System.out.println("Der Magier " + kampfMagier.getName() + " ist tot!");
 }

 }
 }
 ***/
/***for(int i = 1; i <= 10; i++)
 {
 int kriegerSchaden = this.random.nextInt(10);
 this.kampfMagier.veringereLeben(kriegerSchaden);

 System.out.println("Krieger " + this.kampfKrieger.getName() + " macht " + kriegerSchaden + " an " + this.kampfMagier.getName() + "\n");

 if(this.kampfMagier.getLebenspunkte() <=0)
 {
 System.out.println("Der Magier " + kampfMagier.getName() + " ist tot!");
 }

 }
 ***/


